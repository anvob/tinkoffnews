package com.anvob.tinkoffnews.data.datasource;

import com.anvob.tinkoffnews.data.datasource.local.ILocalDataSource;
import com.anvob.tinkoffnews.data.datasource.remote.IRemoteDataSource;
import com.anvob.tinkoffnews.data.db.entities.NewsContentEntity;
import com.anvob.tinkoffnews.data.db.entities.NewsEntity;
import com.anvob.tinkoffnews.data.dto.ContentItem;
import com.anvob.tinkoffnews.data.dto.NewsItem;
import com.anvob.tinkoffnews.data.model.NewsDate;
import com.anvob.tinkoffnews.data.model.NewsDetails;
import com.anvob.tinkoffnews.data.model.NewsListItem;
import com.anvob.tinkoffnews.data.model.NewsTitle;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import io.reactivex.Maybe;
import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;

import static org.mockito.Mockito.when;

public class RepositoryTest {

    private List<NewsListItem> REMOTE_NEWS = new ArrayList<>();
    private List<NewsEntity> DISK_NEWS = new ArrayList<>();

    private NewsContentEntity mNewsContentEntity;
    private NewsDetails mNewsDetails;

    @Mock
    private ILocalDataSource mLocalDataSource;

    @Mock
    private IRemoteDataSource mRemoteDataSource;

    Repository mRepository;

    @Before
    public void setUp() throws Exception {

        MockitoAnnotations.initMocks(this);
        initRemoteNewsEntities();
        initDiskNewsEntities();
        mNewsContentEntity = generateNewsContentEntity();
        mNewsDetails = generateNewsDetails();

        mRepository = new Repository(mLocalDataSource, mRemoteDataSource);
    }

    @Test
    public void getNewsFromDiskCache() {
        when(mLocalDataSource.getNewsList()).thenReturn(Maybe.just(DISK_NEWS));
        when(mRemoteDataSource.getNewsList()).thenReturn(Observable.just(REMOTE_NEWS));
        TestObserver<List<NewsItem>> testObserver = new TestObserver<>();
        mRepository.getNews(false).subscribe(testObserver);
        testObserver.assertValueCount(1);
        testObserver.assertValue(newsItems ->
                newsItems.get(0).getTitle().contains("Disk") &&
                        (newsItems.size() == 5));
    }

    @Test
    public void getNewsFromNetworkRefresh() {
        when(mLocalDataSource.getNewsList()).thenReturn(Maybe.just(DISK_NEWS));
        when(mRemoteDataSource.getNewsList()).thenReturn(Observable.just(REMOTE_NEWS));
        TestObserver<List<NewsItem>> testObserver = new TestObserver<>();
        mRepository.getNews(true).subscribe(testObserver);
        testObserver.assertValueCount(1);
        testObserver.assertValue(newsItems ->
                newsItems.get(0).getTitle().contains("Remote") &&
                        (newsItems.size() == 3));
    }

    @Test
    public void getNewsFromNetworkNoCache() {
        when(mLocalDataSource.getNewsList()).thenReturn(Maybe.just(new ArrayList<>()));
        when(mRemoteDataSource.getNewsList()).thenReturn(Observable.just(REMOTE_NEWS));
        TestObserver<List<NewsItem>> testObserver = new TestObserver<>();
        mRepository.getNews(false).subscribe(testObserver);
        testObserver.assertValueCount(1);
        testObserver.assertValue(newsItems ->
                newsItems.get(0).getTitle().contains("Remote") &&
                        (newsItems.size() == 3));
    }

    @Test
    public void getNewsContentFromDiskCache() {
        when(mLocalDataSource.getNewsContent("")).thenReturn(Maybe.just(mNewsContentEntity));
        when(mRemoteDataSource.getNewsContent("")).thenReturn(Observable.just(mNewsDetails));
        TestObserver<ContentItem> testObserver = new TestObserver<>();
        mRepository.getNewsContent("").subscribe(testObserver);
        testObserver.assertValueCount(1);
        testObserver.assertValue(contentItem ->
                contentItem.getContent().contains("Disk"));
    }
    @Test
    public void getNewsContentFromRemoteNoCache() {
        when(mLocalDataSource.getNewsContent("")).thenReturn(Maybe.empty());
        when(mRemoteDataSource.getNewsContent("")).thenReturn(Observable.just(mNewsDetails));
        TestObserver<ContentItem> testObserver = new TestObserver<>();
        mRepository.getNewsContent("").subscribe(testObserver);
        testObserver.assertValueCount(1);
        testObserver.assertValue(contentItem ->
                contentItem.getContent().contains("Remote"));
    }

    /*
    Generate remote data source items for News
     */
    private void initRemoteNewsEntities() {
        REMOTE_NEWS.clear();
        for (int i = 0; i < 3; i++) {
            NewsListItem item = new NewsListItem();
            item.setId(String.valueOf(i));
            item.setText(String.format("Remote %d", i));
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DAY_OF_MONTH, (int)(Math.random()*30));
            NewsDate date = new NewsDate();
            date.setMilliseconds(calendar.getTime().getTime());
            item.setPublicationDate(date);
            REMOTE_NEWS.add(item);
        }
    }

    /*
    Generate local data source items for News
     */
    private void initDiskNewsEntities() {
        DISK_NEWS.clear();
        for (int i=5; i < 10; i++) {
            NewsEntity newsEntity = new NewsEntity();
            newsEntity.id = String.valueOf(i);
            newsEntity.title = String.format("Disk %d", i);
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DAY_OF_MONTH, (int)(Math.random()*30));
            newsEntity.date = calendar.getTime().getTime();
            DISK_NEWS.add(newsEntity);
        }
    }

    /*
    Generate local data source item for NewsContent
     */
    private NewsContentEntity generateNewsContentEntity() {
        NewsContentEntity entity = new NewsContentEntity();
        entity.Content = "Disk";
        entity.Id = "5";
        entity.Title="Disk";
        return entity;
    }
    /*
    Generate remote data source item for NewsContent
     */
    private NewsDetails generateNewsDetails() {
        NewsDetails newsDetails = new NewsDetails();
        newsDetails.setContent("Remote");
        NewsTitle title = new NewsTitle();
        title.setId("10");
        title.setText("Remote");
        newsDetails.setTitle(title);
        return newsDetails;
    }
}