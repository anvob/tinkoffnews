package com.anvob.tinkoffnews.data.db.entities;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.Date;

@Entity(tableName = "news")
public class NewsEntity {
    @PrimaryKey
    @NonNull
    public String id;

    public String title;

    public long date;
}
