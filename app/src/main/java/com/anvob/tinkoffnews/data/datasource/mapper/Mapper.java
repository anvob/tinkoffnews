package com.anvob.tinkoffnews.data.datasource.mapper;

import java.util.ArrayList;
import java.util.List;

public abstract class Mapper<Model extends Object, Entity extends Object> {

    public abstract Model mapFromEntity(Entity entity);

    public abstract Entity mapToEntity(Model model);

    protected Entity mapToEntityWithCheck(Model model) {
        if (model == null) {
            return null;
        }

        return mapToEntity(model);
    }

    public Model mapFromEntityWithCkeck(Entity entity) {
        if (entity == null) {
            return null;
        }

        return mapFromEntity(entity);
    }

    public List<Entity> mapToEntityList(List<Model> list) {
        List<Entity> result = new ArrayList<Entity>();
        for (Model model : list) {
            result.add(mapToEntityWithCheck(model));
        }
        return result;
    }

    public List<Model> mapFromEntityList(List<Entity> list) {
        List<Model> result = new ArrayList<Model>();
        for (Entity entity : list) {
            result.add(mapFromEntityWithCkeck(entity));
        }
        return result;
    }
}
