package com.anvob.tinkoffnews.data.datasource;

import com.anvob.tinkoffnews.data.dto.ContentItem;
import com.anvob.tinkoffnews.data.dto.NewsItem;

import java.util.List;

import io.reactivex.Maybe;

public interface IRepository {
    Maybe<List<NewsItem>> getNews(boolean fromNetwork);

    Maybe<ContentItem> getNewsContent(String id);
}
