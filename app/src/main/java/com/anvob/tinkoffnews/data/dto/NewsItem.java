package com.anvob.tinkoffnews.data.dto;

import java.util.Date;

public class NewsItem {

    private String mId;

    private String mTitle;

    private Date mDate;

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public Date getDate() {
        return mDate;
    }

    public void setDate(Date date) {
        mDate = date;
    }
}
