package com.anvob.tinkoffnews.data.db.entities;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "newsContent")
public class NewsContentEntity {
    @PrimaryKey
    @NonNull
    public String Id;

    public String Title;

    public String Content;
}
