package com.anvob.tinkoffnews.data.datasource.mapper;

import com.anvob.tinkoffnews.data.db.entities.NewsContentEntity;
import com.anvob.tinkoffnews.data.dto.ContentItem;

public class ContentMapper extends Mapper<ContentItem, NewsContentEntity> {
    @Override
    public ContentItem mapFromEntity(NewsContentEntity entity) {
        ContentItem item = new ContentItem();
        item.setId(entity.Id);
        item.setTitle(entity.Title);
        item.setContent(entity.Content);
        return item;
    }

    @Override
    public NewsContentEntity mapToEntity(ContentItem item) {
        NewsContentEntity entity = new NewsContentEntity();
        entity.Content = item.getContent();
        entity.Title = item.getTitle();
        entity.Id = item.getId();
        return entity;
    }
}
