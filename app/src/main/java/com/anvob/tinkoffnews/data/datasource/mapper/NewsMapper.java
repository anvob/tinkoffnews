package com.anvob.tinkoffnews.data.datasource.mapper;

import com.anvob.tinkoffnews.data.db.entities.NewsEntity;
import com.anvob.tinkoffnews.data.dto.NewsItem;

import java.util.Date;

public class NewsMapper extends Mapper<NewsItem, NewsEntity> {
    @Override
    public NewsItem mapFromEntity(NewsEntity entity) {
        NewsItem item = new NewsItem();
        item.setId(entity.id);
        item.setTitle(entity.title);
        item.setDate(new Date(entity.date));
        return item;
    }

    @Override
    public NewsEntity mapToEntity(NewsItem newsItem) {
        NewsEntity entity = new NewsEntity();
        entity.id = newsItem.getId();
        entity.title = newsItem.getTitle();
        entity.date = newsItem.getDate().getTime();
        return entity;
    }
}
