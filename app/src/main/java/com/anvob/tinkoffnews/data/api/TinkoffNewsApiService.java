package com.anvob.tinkoffnews.data.api;

import com.anvob.tinkoffnews.data.model.NewsDetailsResponse;
import com.anvob.tinkoffnews.data.model.NewsListResponse;

import io.reactivex.Observable;
import io.reactivex.annotations.NonNull;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface TinkoffNewsApiService {

    @GET("/v1/news")
    Observable<NewsListResponse> getNewsList();

    @GET("/v1/news_content")
    Observable<NewsDetailsResponse> getNewsDetails(@NonNull @Query("id") String id);
}
