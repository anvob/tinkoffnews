package com.anvob.tinkoffnews.data.datasource.remote;

import com.anvob.tinkoffnews.data.model.NewsDetails;
import com.anvob.tinkoffnews.data.model.NewsListItem;

import java.util.List;

import io.reactivex.Observable;

public interface IRemoteDataSource {

    Observable<List<NewsListItem>> getNewsList();

    Observable<NewsDetails> getNewsContent(String id);
}
