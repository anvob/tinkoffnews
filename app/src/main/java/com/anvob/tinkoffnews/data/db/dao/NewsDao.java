package com.anvob.tinkoffnews.data.db.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.anvob.tinkoffnews.data.db.entities.NewsEntity;

import java.util.List;

import io.reactivex.Maybe;

@Dao
public interface NewsDao {
    @Query("SELECT * FROM news")
    Maybe<List<NewsEntity>> getAll();

    @Query("SELECT * FROM news WHERE id = :id")
    Maybe<NewsEntity> getById(String id);

    @Insert
    void insert(NewsEntity entity);

    @Update
    void update(NewsEntity entity);

    @Delete
    void delete(NewsEntity entity);

    @Query("DELETE FROM news")
    void deleteAll();
}
