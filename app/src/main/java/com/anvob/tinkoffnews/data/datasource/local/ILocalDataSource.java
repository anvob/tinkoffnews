package com.anvob.tinkoffnews.data.datasource.local;

import com.anvob.tinkoffnews.data.db.entities.NewsContentEntity;
import com.anvob.tinkoffnews.data.db.entities.NewsEntity;

import java.util.List;

import io.reactivex.Maybe;

public interface ILocalDataSource {
    Maybe<List<NewsEntity>> getNewsList();

    Maybe<NewsContentEntity> getNewsContent(String id);

    void saveNews(List<NewsEntity> entityList);

    void saveNews(NewsEntity entity);

    void saveNewsContent(NewsContentEntity entity);

    void clearAll();

    void removeNewsContent(String id);
}
