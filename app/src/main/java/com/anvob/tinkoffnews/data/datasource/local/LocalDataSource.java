package com.anvob.tinkoffnews.data.datasource.local;

import android.content.Context;

import androidx.room.Room;

import com.anvob.tinkoffnews.data.db.AppDatabase;
import com.anvob.tinkoffnews.data.db.dao.NewsContentDao;
import com.anvob.tinkoffnews.data.db.dao.NewsDao;
import com.anvob.tinkoffnews.data.db.entities.NewsContentEntity;
import com.anvob.tinkoffnews.data.db.entities.NewsEntity;

import java.util.List;

import io.reactivex.Maybe;

public class LocalDataSource implements ILocalDataSource {

    private AppDatabase mDatabase;

    public LocalDataSource(Context context){
        mDatabase =  Room.databaseBuilder(context, AppDatabase.class, "appdatabase").fallbackToDestructiveMigration().build();
    }

    @Override
    public Maybe<List<NewsEntity>> getNewsList() {
        NewsDao newsDao = mDatabase.newsDao();
        return newsDao.getAll();
    }

    @Override
    public Maybe<NewsContentEntity> getNewsContent(String id) {
        NewsContentDao newsContentDao = mDatabase.newsContentDao();
        return newsContentDao.getById(id);
    }

    @Override
    public void saveNews(List<NewsEntity> entityList) {
        for (NewsEntity entity:
             entityList) {
            saveNews(entity);
        }
    }

    @Override
    public void saveNews(NewsEntity entity) {
        NewsDao newsDao = mDatabase.newsDao();
        newsDao.insert(entity);
    }

    @Override
    public void saveNewsContent(NewsContentEntity entity) {
        NewsContentDao newsContentDao = mDatabase.newsContentDao();
        newsContentDao.insert(entity);
    }

    @Override
    public void clearAll() {
        NewsDao newsDao = mDatabase.newsDao();
        newsDao.deleteAll();
        NewsContentDao newsContentDao = mDatabase.newsContentDao();
        newsContentDao.deleteAll();;
    }

    @Override
    public void removeNewsContent(String id) {
        NewsContentDao newsContentDao = mDatabase.newsContentDao();
        newsContentDao.deleteById(id);
    }
}
