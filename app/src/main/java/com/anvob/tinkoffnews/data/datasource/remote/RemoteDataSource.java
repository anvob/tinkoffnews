package com.anvob.tinkoffnews.data.datasource.remote;

import com.anvob.tinkoffnews.data.api.TinkoffNewsApiService;
import com.anvob.tinkoffnews.data.model.NewsDetails;
import com.anvob.tinkoffnews.data.model.NewsDetailsResponse;
import com.anvob.tinkoffnews.data.model.NewsListItem;
import com.anvob.tinkoffnews.data.model.NewsListResponse;

import java.util.List;

import io.reactivex.Observable;

public class RemoteDataSource implements IRemoteDataSource {

    private TinkoffNewsApiService mService;

    public RemoteDataSource(TinkoffNewsApiService service) {
        this.mService = service;
    }

    @Override
    public Observable<List<NewsListItem>> getNewsList() {
        return mService.getNewsList()
                .map(NewsListResponse::getPayload);
    }

    @Override
    public Observable<NewsDetails> getNewsContent(String id) {
        return mService.getNewsDetails(id)
                .map(NewsDetailsResponse::getPayload);
    }
}
