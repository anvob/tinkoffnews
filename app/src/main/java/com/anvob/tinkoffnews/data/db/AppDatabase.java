package com.anvob.tinkoffnews.data.db;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.anvob.tinkoffnews.data.db.dao.NewsContentDao;
import com.anvob.tinkoffnews.data.db.dao.NewsDao;
import com.anvob.tinkoffnews.data.db.entities.NewsContentEntity;
import com.anvob.tinkoffnews.data.db.entities.NewsEntity;

@Database(entities = {NewsEntity.class, NewsContentEntity.class}, version = 2, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
    public abstract NewsDao newsDao();
    public abstract NewsContentDao newsContentDao();
}
