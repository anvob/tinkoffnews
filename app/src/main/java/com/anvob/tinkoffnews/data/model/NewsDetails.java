package com.anvob.tinkoffnews.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NewsDetails {
    @SerializedName("title")
    @Expose
    private NewsTitle title;
    @SerializedName("creationDate")
    @Expose
    private NewsDate creationDate;
    @SerializedName("lastModificationDate")
    @Expose
    private NewsDate lastModificationDate;
    @SerializedName("content")
    @Expose
    private String content;
    @SerializedName("bankInfoTypeId")
    @Expose
    private Integer bankInfoTypeId;
    @SerializedName("typeId")
    @Expose
    private String typeId;

    public NewsTitle getTitle() {
        return title;
    }

    public void setTitle(NewsTitle title) {
        this.title = title;
    }

    public NewsDate getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(NewsDate creationDate) {
        this.creationDate = creationDate;
    }

    public NewsDate getLastModificationDate() {
        return lastModificationDate;
    }

    public void setLastModificationDate(NewsDate lastModificationDate) {
        this.lastModificationDate = lastModificationDate;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getBankInfoTypeId() {
        return bankInfoTypeId;
    }

    public void setBankInfoTypeId(Integer bankInfoTypeId) {
        this.bankInfoTypeId = bankInfoTypeId;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }
}
