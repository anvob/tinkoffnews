package com.anvob.tinkoffnews.data.db.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.anvob.tinkoffnews.data.db.entities.NewsContentEntity;

import java.util.List;

import io.reactivex.Maybe;

@Dao
public interface NewsContentDao {
    @Query("SELECT * FROM newsContent")
    Maybe<List<NewsContentEntity>> getAll();

    @Query("SELECT * FROM newsContent WHERE id = :id")
    Maybe<NewsContentEntity> getById(String id);

    @Insert
    void insert(NewsContentEntity employee);

    @Update
    void update(NewsContentEntity employee);

    @Delete
    void delete(NewsContentEntity employee);

    @Query("DELETE FROM newsContent")
    void deleteAll();

    @Query("DELETE FROM newsContent WHERE id = :id")
    void deleteById(String id);
}
