package com.anvob.tinkoffnews.data.datasource;

import com.anvob.tinkoffnews.data.datasource.local.ILocalDataSource;
import com.anvob.tinkoffnews.data.datasource.remote.IRemoteDataSource;
import com.anvob.tinkoffnews.data.dto.ContentItem;
import com.anvob.tinkoffnews.data.dto.NewsItem;
import com.anvob.tinkoffnews.data.datasource.mapper.ContentMapper;
import com.anvob.tinkoffnews.data.datasource.mapper.NewsMapper;
import com.anvob.tinkoffnews.data.model.NewsListItem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import io.reactivex.Maybe;
import io.reactivex.Observable;

public class Repository implements IRepository {
    private ILocalDataSource mLocalDataSource;
    private IRemoteDataSource mRemoteDataSource;

    private NewsMapper mNewsMapper;
    private ContentMapper mContentMapper;

    public Repository(ILocalDataSource localDataSource, IRemoteDataSource remoteDataSource) {
        mLocalDataSource = localDataSource;
        mRemoteDataSource = remoteDataSource;
        mNewsMapper = new NewsMapper();
        mContentMapper = new ContentMapper();
    }

    @Override
    public Maybe<List<NewsItem>> getNews(boolean fromNetwork) {
        return Observable.just(fromNetwork)
                .flatMap(needUpdate ->  {
                    if(needUpdate) {
                        return getNewsFromNetwork();
                    }
                    return Observable.concat(getNewsFromDisk().toObservable(), getNewsFromNetwork());
                })

                .filter(list -> !list.isEmpty())
         .firstElement();
    }

    private Observable<List<NewsItem>> getNewsFromNetwork() {
            return mRemoteDataSource.getNewsList()
                    .map(remoteNewsList -> {
                        List<NewsItem> itemList = new ArrayList<>();
                        for (NewsListItem newsApiItem: remoteNewsList) {
                            NewsItem item = new NewsItem();
                            item.setId(newsApiItem.getId());
                            item.setTitle(newsApiItem.getText());
                            item.setDate(new Date(newsApiItem.getPublicationDate().getMilliseconds()));
                            itemList.add(item);
                        }
                        return itemList;
                    })
                    .map(unsortedList -> {
                        List<NewsItem> sortedList = new ArrayList<>(unsortedList);
                        Collections.sort(sortedList, Collections.reverseOrder((o1, o2) -> o1.getDate().compareTo(o2.getDate())));
                        return sortedList;
                    })
                    .doOnNext(item -> {
                        mLocalDataSource.clearAll();
                        mLocalDataSource.saveNews(mNewsMapper.mapToEntityList(item));
                    });
    }

    private Maybe<List<NewsItem>> getNewsFromDisk() {
        return mLocalDataSource.getNewsList()
                .map(entityList -> mNewsMapper.mapFromEntityList(entityList));
    }

    @Override
    public Maybe<ContentItem> getNewsContent(String id) {
        return Observable.concat(getNewsContentFromDisk(id), getNewsContentFromNetwork(id))
                .firstElement();
    }

    private Observable<ContentItem> getNewsContentFromNetwork(String id) {
        return mRemoteDataSource.getNewsContent(id)
                .map(apiItem -> {
                    ContentItem item = new ContentItem();
                    item.setContent(apiItem.getContent());
                    item.setTitle(apiItem.getTitle().getText());
                    item.setId(apiItem.getTitle().getId());
                    return item;
                })
                .doOnNext(item -> {
                    mLocalDataSource.removeNewsContent(item.getId());
                    mLocalDataSource.saveNewsContent(mContentMapper.mapToEntity(item));
                });
    }

    private Observable<ContentItem> getNewsContentFromDisk(String id) {
        return mLocalDataSource.getNewsContent(id)
                .map(entity -> mContentMapper.mapFromEntity(entity)).toObservable();
    }
}
