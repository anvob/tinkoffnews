package com.anvob.tinkoffnews.tools;

import android.util.TypedValue;

public class Utils {
    public static int dpToPx(int dp) {
        return Math.round((TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, android.content.res.Resources.getSystem().getDisplayMetrics())));
    }
}
