package com.anvob.tinkoffnews.tools.schedulers;

import io.reactivex.Scheduler;

public interface SchedulersProvider {
    Scheduler ui();

    Scheduler computation();

    Scheduler trampoline();

    Scheduler newThread();

    Scheduler io();
}
