package com.anvob.tinkoffnews.ui.newslist;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.anvob.tinkoffnews.R;
import com.anvob.tinkoffnews.data.dto.NewsItem;
import com.anvob.tinkoffnews.presentation.newslist.NewsListPresenter;

import java.util.ArrayList;
import java.util.List;

public class NewsListAdapter extends RecyclerView.Adapter<NewsItemVH> {

    private NewsListPresenter mPresenter;
    public NewsListAdapter(NewsListPresenter presenter) {
        this.mPresenter = presenter;
    }

    List<NewsItem> items = new ArrayList<>();

    @NonNull
    @Override
    public NewsItemVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        NewsItemVH vh = new NewsItemVH(inflater.inflate(R.layout.item_news, parent, false));
        vh.itemView.setOnClickListener(v -> {
            if(vh.getAdapterPosition() != RecyclerView.NO_POSITION) {
                mPresenter.onItemClick(items.get(vh.getAdapterPosition()));
            }
        });
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull NewsItemVH holder, int position) {
        holder.fill(items.get(position));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setData(List<NewsItem> newsItems) {
        items.clear();
        items.addAll(newsItems);
        notifyDataSetChanged();
    }
}
