package com.anvob.tinkoffnews.ui.newsdetails;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;

import com.anvob.tinkoffnews.App;
import com.anvob.tinkoffnews.R;
import com.anvob.tinkoffnews.data.datasource.IRepository;
import com.anvob.tinkoffnews.data.dto.ContentItem;
import com.anvob.tinkoffnews.presentation.newsdetails.NewsContentPresenter;
import com.anvob.tinkoffnews.presentation.newsdetails.NewsContentView;
import com.anvob.tinkoffnews.tools.schedulers.SchedulersProvider;
import com.anvob.tinkoffnews.ui.common.BaseFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;

import javax.inject.Inject;

import ru.terrakok.cicerone.Router;
import toothpick.Toothpick;

public class NewsContentFragment extends BaseFragment implements NewsContentView {

    private WebView mBodyWebView;

    private static final String SCROLL_POSITION = "scrollPosition";

    @Inject
    Router mRouter;

    @Inject
    SchedulersProvider mSchedulersProvider;

    @Inject
    IRepository mRepository;

    @InjectPresenter
    NewsContentPresenter mPresenter;

    private static final String CONTENT_ID = "content_id";

    public static NewsContentFragment newInstance(String id){
        Bundle bundle = new Bundle();
        bundle.putString(CONTENT_ID, id);
        NewsContentFragment fragment = new NewsContentFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @ProvidePresenter
    NewsContentPresenter createNewsContentPresenter() {
        return new NewsContentPresenter(mRouter, mSchedulersProvider, mRepository, getArguments().getString(CONTENT_ID));
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_news_content;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Toothpick.inject(this, App.getAppScope());
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final float scrollPosition = savedInstanceState == null ? 0f : savedInstanceState.getFloat(SCROLL_POSITION);
        View view = super.onCreateView(inflater, container, savedInstanceState);
        Toolbar mToolbar = view.findViewById(R.id.toolbar);
        mToolbar.setTitle(R.string.news_content_fragemnt_title);
        mToolbar.setNavigationOnClickListener(v -> mPresenter.onBackClicked());
        mToolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        mBodyWebView = view.findViewById(R.id.body);
        mBodyWebView.setBackgroundColor(Color.TRANSPARENT);
        mBodyWebView.setWebViewClient(new WebViewClient() {
            @Override @SuppressWarnings("deprecation") public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                if (scrollPosition > 0) {
                    view.setScrollY((int) (view.getContentHeight() * view.getScale() * scrollPosition));
                }
            }
        });
        return view;
    }

    @Override
    public void setData(ContentItem data) {
        mBodyWebView.loadDataWithBaseURL(null,
                "<style>img{display: inline; height: auto; max-width: 100%;}</style><b>" + data.getTitle() + "</b><br>" + data.getContent(),
                "text/html",
                "UTF-8",
                null);
    }

    @Override
    public void onErrorRefreshClick() {
        mPresenter.getNewsContent();
    }

    @Override
    public void onBackPressed() {
        mPresenter.onBackClicked();
    }

    @Override @SuppressWarnings("deprecation")
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        final float position = mBodyWebView.getScrollY() / (mBodyWebView.getContentHeight() * mBodyWebView.getScale());
        outState.putFloat(SCROLL_POSITION, position);
    }
}
