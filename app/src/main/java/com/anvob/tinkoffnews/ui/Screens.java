package com.anvob.tinkoffnews.ui;

import android.content.Context;
import android.content.Intent;

import androidx.fragment.app.Fragment;

import com.anvob.tinkoffnews.ui.newsdetails.NewsContentFragment;
import com.anvob.tinkoffnews.ui.newslist.NewsListFragment;

import ru.terrakok.cicerone.android.support.SupportAppScreen;

public class Screens {

    public static final class NewsScreen extends SupportAppScreen {
        @Override
        public Fragment getFragment() {
            return new NewsListFragment();
        }
    }

    public static final class NewsContentScreen extends SupportAppScreen {

        private String mContentId;
        public NewsContentScreen(String contentId) {
            this.mContentId = contentId;
        }

        @Override
        public Fragment getFragment() {
            return NewsContentFragment.newInstance(mContentId);
        }
    }
}
