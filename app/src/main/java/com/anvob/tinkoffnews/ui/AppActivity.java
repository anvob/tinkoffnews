package com.anvob.tinkoffnews.ui;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.anvob.tinkoffnews.App;
import com.anvob.tinkoffnews.R;
import com.arellomobile.mvp.MvpAppCompatActivity;

import javax.inject.Inject;

import ru.terrakok.cicerone.Navigator;
import ru.terrakok.cicerone.NavigatorHolder;
import ru.terrakok.cicerone.android.support.SupportAppNavigator;
import ru.terrakok.cicerone.commands.BackTo;
import ru.terrakok.cicerone.commands.Command;
import ru.terrakok.cicerone.commands.Replace;
import toothpick.Toothpick;

public class AppActivity extends MvpAppCompatActivity {

    @Inject
    public NavigatorHolder navigatorHolder;

    private Navigator navigator = new SupportAppNavigator(this, R.id.container) {
        @Override
        protected void setupFragmentTransaction(Command command, Fragment currentFragment, Fragment nextFragment, FragmentTransaction fragmentTransaction) {
            super.setupFragmentTransaction(command, currentFragment, nextFragment, fragmentTransaction);
            //fix incorrect order lifecycle callback of MainFlowFragment
            fragmentTransaction.setReorderingAllowed(true);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Toothpick.inject(this, App.getAppScope());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_container);

        if (savedInstanceState == null) {
            navigator.applyCommands(new Command[] {
                    new BackTo(null),
                    new Replace(new Screens.NewsScreen())});
        }
    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
        navigatorHolder.setNavigator(navigator);
    }

    @Override
    protected void onPause() {
        navigatorHolder.removeNavigator();
        super.onPause();
    }
}
