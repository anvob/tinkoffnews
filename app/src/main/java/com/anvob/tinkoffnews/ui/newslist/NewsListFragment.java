package com.anvob.tinkoffnews.ui.newslist;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.anvob.tinkoffnews.App;
import com.anvob.tinkoffnews.R;
import com.anvob.tinkoffnews.data.datasource.IRepository;
import com.anvob.tinkoffnews.data.dto.NewsItem;
import com.anvob.tinkoffnews.presentation.newslist.NewsListPresenter;
import com.anvob.tinkoffnews.presentation.newslist.NewsListView;
import com.anvob.tinkoffnews.tools.schedulers.SchedulersProvider;
import com.anvob.tinkoffnews.ui.common.BaseFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;

import java.util.List;

import javax.inject.Inject;

import ru.terrakok.cicerone.Router;
import toothpick.Toothpick;

public class NewsListFragment extends BaseFragment implements NewsListView {

    private SwipeRefreshLayout mSwipeRefreshLayout;

    @Inject
    Router mRouter;

    @Inject
    SchedulersProvider mSchedulersProvider;

    @Inject
    IRepository mRepository;

    @InjectPresenter
    NewsListPresenter mPresenter;

    private NewsListAdapter mAdapter;

    @ProvidePresenter
    NewsListPresenter createNewsListPresenter() {
        return new NewsListPresenter(mRouter, mSchedulersProvider, mRepository);
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_list;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Toothpick.inject(this, App.getAppScope());
        super.onCreate(savedInstanceState);
        mAdapter = new NewsListAdapter(mPresenter);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        Toolbar mToolbar = view.findViewById(R.id.toolbar);
        mToolbar.setTitle(R.string.news_fragment_title);
        mSwipeRefreshLayout = view.findViewById(R.id.swiperefresh);
        mSwipeRefreshLayout.setOnRefreshListener(() -> mPresenter.getNews(true));
        RecyclerView mRecyclerView = view.findViewById(R.id.list);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL));
        mRecyclerView.setAdapter(mAdapter);
        return view;
    }

    @Override
    public void onDestroyView() {
        mSwipeRefreshLayout.setOnRefreshListener(null);
        super.onDestroyView();
    }
    @Override
    public void onErrorRefreshClick() {
        mPresenter.getNews(true);
    }


    @Override
    public void setData(List<NewsItem> items) {
        mAdapter.setData(items);
    }

    @Override
    public void showProgressBar(boolean isShow) {
        mSwipeRefreshLayout.setRefreshing(isShow);
    }

    @Override
    public void onBackPressed() {
        mPresenter.onBackClicked();
    }
}
