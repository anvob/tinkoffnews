package com.anvob.tinkoffnews.ui.newslist;

import android.text.Html;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.anvob.tinkoffnews.R;
import com.anvob.tinkoffnews.data.dto.NewsItem;

import java.text.SimpleDateFormat;
import java.util.Locale;

public class NewsItemVH extends RecyclerView.ViewHolder {

    private AppCompatTextView tvTitle;
    private AppCompatTextView tvDate;
    private SimpleDateFormat dateFormat;

    public NewsItemVH(@NonNull View itemView) {
        super(itemView);
        dateFormat= new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
        tvTitle = itemView.findViewById(R.id.title);
        tvDate = itemView.findViewById(R.id.date);
    }

    public void fill(NewsItem item) {
        if(item == null) return;
        tvTitle.setText(Html.fromHtml(item.getTitle()));
        if(item.getDate() != null) {
            tvDate.setText(dateFormat.format(item.getDate()));
        }
    }
}
