package com.anvob.tinkoffnews.ui.common;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;

import com.anvob.tinkoffnews.R;
import com.anvob.tinkoffnews.presentation.common.BaseView;
import com.anvob.tinkoffnews.tools.Utils;
import com.arellomobile.mvp.MvpAppCompatFragment;
import com.google.android.material.snackbar.Snackbar;

public abstract class BaseFragment extends MvpAppCompatFragment implements BaseView {

    protected abstract int getLayoutRes();
    private FrameLayout mPreloaderView;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutRes(), container, false);
        mPreloaderView = createPreloaderView();
        ((ViewGroup)view).addView(mPreloaderView);
        return view;
    }

    public void onBackPressed() {

    }

    public void showError(String message) {
        Snackbar.make(getView(), message, Snackbar.LENGTH_INDEFINITE)
                .setActionTextColor(ContextCompat.getColor(getContext(), R.color.colorPrimary))
                .setAction(getString(R.string.refresh), v -> onErrorRefreshClick()).show();
    }

    public void onErrorRefreshClick(){

    }

    public void showProgressBar(boolean isShow) {
        if(mPreloaderView != null) {
            mPreloaderView.setVisibility(isShow ? View.VISIBLE : View.GONE);
        }
    }

    private FrameLayout createPreloaderView()
    {
        CoordinatorLayout.LayoutParams pb = new CoordinatorLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        FrameLayout preloaderView = new FrameLayout(getContext());
        preloaderView.setClickable(true);
        preloaderView.setBackground(new ColorDrawable(Color.parseColor("#22FFFFFF")));
        preloaderView.setLayoutParams(pb);
        preloaderView.setVisibility(View.GONE);

        ProgressBar progressBar = new ProgressBar(getContext(), null, android.R.attr.progressBarStyle);
        FrameLayout.LayoutParams p = new FrameLayout.LayoutParams(Utils.dpToPx(70), Utils.dpToPx(70));
        p.gravity = Gravity.CENTER;
        progressBar.setLayoutParams(p);
        progressBar.setVisibility(View.VISIBLE);

        preloaderView.addView(progressBar);
        return preloaderView;
    }
}
