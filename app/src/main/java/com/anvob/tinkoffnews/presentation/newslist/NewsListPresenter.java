package com.anvob.tinkoffnews.presentation.newslist;

import com.anvob.tinkoffnews.data.datasource.IRepository;
import com.anvob.tinkoffnews.data.dto.NewsItem;
import com.anvob.tinkoffnews.presentation.common.BasePresenter;
import com.anvob.tinkoffnews.tools.schedulers.SchedulersProvider;
import com.anvob.tinkoffnews.ui.Screens;
import com.arellomobile.mvp.InjectViewState;

import org.reactivestreams.Subscription;

import io.reactivex.disposables.Disposable;
import ru.terrakok.cicerone.Router;

@InjectViewState
public class NewsListPresenter extends BasePresenter<NewsListView> {
    private Router mRouter;
    private SchedulersProvider mSchedulersProvider;
    private IRepository mRepository;

    public NewsListPresenter(Router router, SchedulersProvider provider, IRepository repository) {
        this.mRouter = router;
        this.mSchedulersProvider = provider;
        this.mRepository = repository;
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        getNews(false);
    }

    public void getNews(boolean fromNetwork) {
        getViewState().showProgressBar(true);
        Disposable subscription = mRepository.getNews(fromNetwork)
                .subscribeOn(mSchedulersProvider.io())
                .observeOn(mSchedulersProvider.ui())
                .subscribe(res -> {
                            getViewState().setData(res);
                            getViewState().showProgressBar(false);
                        },
                        this::onError);
        addSubscription(subscription);
    }

    public void onBackClicked() {
        mRouter.exit();
    }

    public void onItemClick(NewsItem item) {
        mRouter.navigateTo(new Screens.NewsContentScreen(item.getId()));
    }
}
