package com.anvob.tinkoffnews.presentation.common;

import com.arellomobile.mvp.MvpPresenter;
import com.arellomobile.mvp.MvpView;

import java.io.IOException;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public class BasePresenter<V extends BaseView> extends MvpPresenter<V> {

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Override
    public void onDestroy() {
        compositeDisposable.dispose();
    }

    protected void addSubscription(Disposable disposable) {
        compositeDisposable.add(disposable);
    }

    protected void onError(Throwable throwable) {
        if (throwable instanceof IOException){
            getViewState().showError("Network Error");
        } else {
            getViewState().showError("Unknown Error");
        }
        getViewState().showProgressBar(false);
    }
}
