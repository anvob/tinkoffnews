package com.anvob.tinkoffnews.presentation.newsdetails;

import com.anvob.tinkoffnews.data.dto.ContentItem;
import com.anvob.tinkoffnews.presentation.common.BaseView;

public interface NewsContentView extends BaseView {

    void setData(ContentItem data);
}
