package com.anvob.tinkoffnews.presentation.newsdetails;

import androidx.annotation.NonNull;

import com.anvob.tinkoffnews.data.datasource.IRepository;
import com.anvob.tinkoffnews.presentation.common.BasePresenter;
import com.anvob.tinkoffnews.tools.schedulers.SchedulersProvider;
import com.arellomobile.mvp.InjectViewState;

import io.reactivex.disposables.Disposable;
import ru.terrakok.cicerone.Router;

@InjectViewState
public class NewsContentPresenter extends BasePresenter<NewsContentView> {
    private Router mRouter;
    private SchedulersProvider mSchedulersProvider;
    private IRepository mRepository;
    private String mContentId;

    public NewsContentPresenter(@NonNull Router router, @NonNull SchedulersProvider provider, @NonNull IRepository repository, @NonNull String contentId) {
        this.mRouter = router;
        this.mSchedulersProvider = provider;
        this.mRepository = repository;
        this.mContentId = contentId;
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        getNewsContent();
    }

    public void getNewsContent() {
        getViewState().showProgressBar(true);
        Disposable subscription = mRepository.getNewsContent(mContentId)
                .subscribeOn(mSchedulersProvider.io())
                .observeOn(mSchedulersProvider.ui())
                .subscribe(res -> {
                            getViewState().setData(res);
                            getViewState().showProgressBar(false);
                        },
                        this::onError,
                        ()-> {
                            getViewState().showProgressBar(false);
        });
        addSubscription(subscription);
    }

    public void onBackClicked() {
        mRouter.exit();
    }

}
