package com.anvob.tinkoffnews.presentation.newslist;

import com.anvob.tinkoffnews.data.dto.NewsItem;
import com.anvob.tinkoffnews.presentation.common.BaseView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

import java.util.List;

public interface NewsListView extends BaseView {
    @StateStrategyType(AddToEndSingleStrategy.class)
    void setData(List<NewsItem> items);
}
