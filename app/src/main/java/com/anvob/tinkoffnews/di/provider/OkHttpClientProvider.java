package com.anvob.tinkoffnews.di.provider;

import android.content.Context;

import javax.inject.Inject;
import javax.inject.Provider;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;

public class OkHttpClientProvider implements Provider<OkHttpClient> {

    @Inject
    OkHttpClientProvider() {

    }

    @Override
    public OkHttpClient get() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .build();
    }
}
