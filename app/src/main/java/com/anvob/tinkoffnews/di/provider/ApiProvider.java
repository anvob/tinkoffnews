package com.anvob.tinkoffnews.di.provider;

import com.anvob.tinkoffnews.BuildConfig;
import com.anvob.tinkoffnews.data.api.TinkoffNewsApiService;
import com.google.gson.Gson;

import javax.inject.Inject;
import javax.inject.Provider;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiProvider implements Provider<TinkoffNewsApiService> {

    private OkHttpClient mOkHttpClient;

    private Gson mGson;

    @Inject
    ApiProvider(OkHttpClient okHttpClient, Gson gson) {
        mOkHttpClient = okHttpClient;
        mGson = gson;
    }

    @Override
    public TinkoffNewsApiService get() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return new Retrofit.Builder()
                .baseUrl(BuildConfig.API_ENDPOINT)
                .client(mOkHttpClient)
                .addConverterFactory(GsonConverterFactory.create(mGson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
                .create(TinkoffNewsApiService.class);
    }
}
