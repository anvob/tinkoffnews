package com.anvob.tinkoffnews.di.module;

import android.content.Context;

import com.anvob.tinkoffnews.data.datasource.IRepository;
import com.anvob.tinkoffnews.di.provider.RepositoryProvider;
import com.anvob.tinkoffnews.tools.schedulers.AppSchedulers;
import com.anvob.tinkoffnews.tools.schedulers.SchedulersProvider;

import ru.terrakok.cicerone.Cicerone;
import ru.terrakok.cicerone.NavigatorHolder;
import ru.terrakok.cicerone.Router;
import toothpick.config.Module;

public class AppModule extends Module {

    private final Context mContext;

    public AppModule(Context context) {
        this.mContext = context;
        bind(Context.class).toInstance(mContext);
        bind(SchedulersProvider.class).toInstance(new AppSchedulers());

        Cicerone<Router> cicerone = Cicerone.create();
        bind(Router.class).toInstance(cicerone.getRouter());
        bind(NavigatorHolder.class).toInstance(cicerone.getNavigatorHolder());
        bind(IRepository.class).toProvider(RepositoryProvider.class).providesSingletonInScope();
    }

}
