package com.anvob.tinkoffnews.di.module;

import com.anvob.tinkoffnews.data.api.TinkoffNewsApiService;
import com.anvob.tinkoffnews.di.provider.ApiProvider;
import com.anvob.tinkoffnews.di.provider.GsonProvider;
import com.anvob.tinkoffnews.di.provider.OkHttpClientProvider;
import com.google.gson.Gson;

import okhttp3.OkHttpClient;
import toothpick.config.Module;

public class NetworkModule extends Module {

    public NetworkModule() {
        bind(OkHttpClient.class).toProvider(OkHttpClientProvider.class).providesSingletonInScope();
        bind(Gson.class).toProvider(GsonProvider.class).providesSingletonInScope();
        bind(TinkoffNewsApiService.class).toProvider(ApiProvider.class).providesSingletonInScope();
    }
}
