package com.anvob.tinkoffnews.di.provider;

import android.content.Context;

import com.anvob.tinkoffnews.data.api.TinkoffNewsApiService;
import com.anvob.tinkoffnews.data.datasource.IRepository;
import com.anvob.tinkoffnews.data.datasource.Repository;
import com.anvob.tinkoffnews.data.datasource.local.LocalDataSource;
import com.anvob.tinkoffnews.data.datasource.remote.RemoteDataSource;

import javax.inject.Inject;
import javax.inject.Provider;

public class RepositoryProvider implements Provider<IRepository> {

    private Context mContext;

    private TinkoffNewsApiService mService;

    @Inject
    RepositoryProvider(Context context, TinkoffNewsApiService service) {
        mService = service;
        mContext = context;
    }

    @Override
    public IRepository get() {
        return new Repository(new LocalDataSource(mContext), new RemoteDataSource(mService));
    }
}
