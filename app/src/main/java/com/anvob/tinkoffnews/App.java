package com.anvob.tinkoffnews;

import android.app.Application;

import androidx.annotation.NonNull;

import com.anvob.tinkoffnews.di.module.AppModule;
import com.anvob.tinkoffnews.di.module.NetworkModule;

import toothpick.Scope;
import toothpick.Toothpick;
import toothpick.configuration.Configuration;
import toothpick.registries.FactoryRegistryLocator;
import toothpick.registries.MemberInjectorRegistryLocator;

public class App extends Application {

    public static final String APP_SCOPE = "app_scope";

    private static Scope sAppScope;

    @NonNull
    public static Scope getAppScope() {
        return sAppScope;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        initToothpick();
        initAppScope();
    }
    private void initToothpick() {
        if (BuildConfig.DEBUG) {
            Toothpick.setConfiguration(Configuration.forDevelopment().preventMultipleRootScopes());
        } else {
            Toothpick.setConfiguration(Configuration.forProduction().disableReflection());
            FactoryRegistryLocator.setRootRegistry(new com.anvob.tinkoffnews.FactoryRegistry());
            MemberInjectorRegistryLocator.setRootRegistry(new com.anvob.tinkoffnews.MemberInjectorRegistry());
        }
    }

    private void initAppScope() {
        sAppScope = Toothpick.openScope(APP_SCOPE);
        sAppScope.installModules(new AppModule(this), new NetworkModule());
    }
}
